package com.wujunshen.entity;

import lombok.Data;

/**
 * Author:frankwoo(吴峻申) <br>
 * Date:2017/9/15 <br>
 * Time:上午11:51 <br>
 * Mail:frank_wjs@hotmail.com <br>
 */
@Data
public class RedisNode {
    private String hostName;
    private int port;
}