package com.wujunshen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.wujunshen.*")
public class RedisUtilsApplication {
    public static void main(String[] args) {
        SpringApplication.run(RedisUtilsApplication.class, args);
    }
}
