package com.wujunshen.util.redis;

import com.wujunshen.entity.RedisNode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.data.redis.connection.SortParameters;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.query.SortQueryBuilder;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Redis 集群实现了单机 Redis 中所有处理单个数据库键的命令。
 * <p/>
 * 针对多个数据库键的复杂计算操作， 比如集合的并集操作、合集操作没有被实现， 那些理论上需要使用多个节点的多个数据库键才能完成的命令也没有被实现。
 * <p/>
 * 在将来， 用户也许可以通过 MIGRATE COPY 命令， 在集群的计算节点（computation node）中执行针对多个数据库键的只读操作，但集群本身不会去实现那些需要将多个数据库键在多个节点中移来移去的复杂多键命令。
 * <p/>
 * Redis 集群不像单机 Redis 那样支持多数据库功能， 集群只使用默认的0号数据库， 并且不能使用 SELECT 命令<br>
 * User:Administrator(吴峻申) <br>
 * Date:2016-8-11 <br>
 * Time:14:57 <br>
 * Mail:frank_wjs@hotmail.com <br>
 */
@Component
@Slf4j
@NoArgsConstructor
@EnableAutoConfiguration
public class RedisUtils {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private JedisConnectionFactory connectionFactory;

    /**
     * 获取redis节点信息
     *
     * @return
     */
    public RedisNode getNodes() {
        RedisNode redisNode = new RedisNode();
        redisNode.setHostName(connectionFactory.getHostName());
        redisNode.setPort(connectionFactory.getPort());

        return redisNode;
    }


    /**
     * 清空redis缓存数据（慎用）
     *
     * @return 结果
     */
    public String flushDB() {
        connectionFactory.getConnection().flushDb();
        return "ok";
    }

    /**
     * 目前集群模式无法支持管道模式，会报异常：
     * Pipeline is currently not supported for JedisClusterConnection.
     * <p/>
     * 执行管道
     *
     * @param key
     * @return
     */
    public List<Object> executePipelined(final String key) {
        final RedisSerializer<String> serializer = stringRedisTemplate.getStringSerializer();
        final Long listSize = stringRedisTemplate.opsForList().size(key);

        return stringRedisTemplate.executePipelined((RedisCallback<String>) connection -> {
            for (int i = 0; i < listSize; i++) {
                byte[] listName = serializer.serialize(key);
                connection.rPop(listName);
            }
            return null;
        }, serializer);
    }

    /**
     * 如果key值不存在，则存储
     *
     * @param key
     * @param value
     */
    public void setIfAbsent(String key, String value) {
        stringRedisTemplate.opsForValue().setIfAbsent(key, value);
    }

    /**
     * 添加value值在原有value值后面
     *
     * @param key
     * @param value
     */
    public int append(String key, String value) {
        return stringRedisTemplate.opsForValue().append(key, value);
    }

    /**
     * 将键值改名
     *
     * @param oldKey
     * @param newKey
     */
    public void rename(String oldKey, String newKey) {
        stringRedisTemplate.rename(oldKey, newKey);
    }

    /**
     * 检查是否存在该key值
     *
     * @param key
     * @return
     */
    public boolean exists(final String key) {
        return stringRedisTemplate.hasKey(key);
    }

    /**
     * 添加key和value
     *
     * @param key
     * @param value
     */
    public void set(String key, String value) {
        stringRedisTemplate.opsForValue().set(key, value);
    }

    /**
     * 根据key值查询value
     *
     * @param key
     * @return
     */
    public String getBy(String key) {
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 添加key和value时候，设定失效时长
     *
     * @param key
     * @param value
     * @param expireTime
     */
    public void setExpire(String key, String value, Long expireTime) {
        stringRedisTemplate.opsForValue().set(key, value, expireTime, TimeUnit.SECONDS);
    }

    /**
     * 批量添加key和value
     *
     * @param map
     */
    public void multiSet(Map<String, String> map) {
        stringRedisTemplate.opsForValue().multiSet(map);
    }

    /**
     * 批量查询value
     *
     * @param keys
     * @return
     */
    public List<String> multiGet(Set<String> keys) {
        return stringRedisTemplate.opsForValue().multiGet(keys);
    }

    /**
     * 批量删除key
     *
     * @param keys
     */
    public void deleteMulti(Set<String> keys) {
        stringRedisTemplate.delete(keys);
    }

    /**
     * 删除key
     *
     * @param key
     */
    public void delete(String key) {
        stringRedisTemplate.delete(key);
    }

    /**
     * 获取value时候，更新新的value值
     *
     * @param key
     * @param value
     */
    public void getAndSet(String key, String value) {
        stringRedisTemplate.opsForValue().getAndSet(key, value);
    }

    /**
     * 根据strIndex和endIndex,截取value值
     *
     * @param key
     * @param strIndex
     * @param endIndex -1为获取所有value值
     * @return
     */
    public String getRangeOf(String key, long strIndex, long endIndex) {
        return stringRedisTemplate.opsForValue().get(key, strIndex, endIndex);
    }

    /**
     * 获取所有key
     *
     * @param patternKey
     * @return
     */
    public Collection<String> getKeys(String patternKey) {
        return stringRedisTemplate.keys(patternKey);
    }

    /**
     * 查询所有value
     *
     * @param patternKey
     * @return
     */
    public List<String> getAllValuesBy(String patternKey) {
        Set<String> keys = (Set<String>) getKeys(patternKey);

        return stringRedisTemplate.opsForValue().multiGet(keys);
    }

    /**
     * 写入list，并设置失效时长
     *
     * @param key   设备号
     * @param value 命令实体类
     */
    public void addElementOfList(final String key, String value) {
        stringRedisTemplate.opsForList().leftPush(key, value);
    }

    /**
     * 每次读取一条（出栈）
     *
     * @param key redis的键
     * @return redis被读取出的值对象
     */
    public Object getElementOfListBy(String key) {
        return stringRedisTemplate.opsForList().rightPop(key);
    }

    /**
     * 读取范围内的element值，endIndex若为-1，则为读取所有element
     *
     * @param key
     * @param strIndex
     * @param endIndex
     * @return
     */
    public List<String> getListBy(String key, int strIndex, int endIndex) {
        return stringRedisTemplate.opsForList().range(key, strIndex, endIndex);
    }

    /**
     * 根据键值获取命令队列
     *
     * @param key redis的键
     * @return 命令队列
     */
    public Long getSizeOfList(String key) {
        return stringRedisTemplate.opsForList().size(key);
    }

    /**
     * list/set排序
     *
     * @param key
     * @return
     */
    public List<String> sortBy(String key) {
        SortQueryBuilder<String> builder = SortQueryBuilder.sort(key);
        builder.alphabetical(true);//对字符串使用“字典顺序”

        return stringRedisTemplate.sort(builder.build());
    }

    /**
     * 外部设置sort条件，传入作为方法参数
     *
     * @param builder
     * @return
     */
    public List<String> sortBy(SortQueryBuilder<String> builder) {
        return stringRedisTemplate.sort(builder.build());
    }

    /**
     * 排序完，存储至新的集合中
     *
     * @param builder
     * @param storeKey
     * @return
     */
    public Long sortByAndStore(SortQueryBuilder<String> builder, String storeKey) {
        return stringRedisTemplate.sort(builder.build(), storeKey);
    }

    /**
     * 实现分页排序
     *
     * @param key
     * @param strIndex
     * @param endIndex
     * @return
     */
    public List<String> sortPageBy(String key, long strIndex, long endIndex) {
        SortQueryBuilder<String> builder = SortQueryBuilder.sort(key);
        builder.alphabetical(true);//对字符串使用“字典顺序”
        builder.limit(strIndex, endIndex);

        return stringRedisTemplate.sort(builder.build());
    }

    /**
     * 降序排序
     *
     * @param key
     * @return
     */
    public List<String> sortDescBy(String key) {
        SortQueryBuilder<String> builder = SortQueryBuilder.sort(key);
        builder.alphabetical(true);//对字符串使用“字典顺序”
        builder.order(SortParameters.Order.DESC);

        return stringRedisTemplate.sort(builder.build());
    }

    /**
     * 根据strIndex和endIndex,截取list的element值
     *
     * @param key
     * @param strIndex
     * @param endIndex -1为获取所有value值
     * @return
     */
    public List<String> getRangeOfList(String key, long strIndex, long endIndex) {
        return stringRedisTemplate.opsForList().range(key, strIndex, endIndex);
    }

    /**
     * 根据键值,获取value的类型
     *
     * @param key
     */
    public String getKindOfValue(String key) {
        return stringRedisTemplate.type(key).code();
    }

    /**
     * 修改list中某个element的值
     *
     * @param key
     * @param value
     * @param index
     */
    public void setElementOfList(String key, String value, long index) {
        stringRedisTemplate.opsForList().set(key, index, value);
    }

    /**
     * 获取list指定index的值
     *
     * @param key
     * @param index
     * @return
     */
    public String getElementOfList(String key, long index) {
        return stringRedisTemplate.opsForList().index(key, index);
    }

    /**
     * 删除list指定index的值
     *
     * @param key
     * @param value
     * @param index
     */
    public void removeElementOfList(String key, String value, long index) {
        stringRedisTemplate.opsForList().remove(key, index, value);
    }

    /**
     * 删除区间以外的数据
     *
     * @param key
     * @param strIndex
     * @param endIndex
     */
    public void removeElementOfListExcept(String key, long strIndex, long endIndex) {
        stringRedisTemplate.opsForList().trim(key, strIndex, endIndex);
    }

    /**
     * 添加元素到sortedSet
     *
     * @param key
     * @param value
     */
    public void addElementOfZSet(String key, String value, double score) {
        stringRedisTemplate.opsForZSet().add(key, value, score);
    }

    /**
     * 从sortedSet获取所有元素(按照score正序排序)
     *
     * @param key
     * @return
     */
    public Set<String> getElementOfZSet(String key, long strIndex, long endIndex) {
        return stringRedisTemplate.opsForZSet().range(key, strIndex, endIndex);
    }

    /**
     * 从sortedSet获取所有元素(按照score倒序排序)
     *
     * @param key
     * @return
     */
    public Set<String> getElementOfZSetDesc(String key, long strIndex, long endIndex) {
        return stringRedisTemplate.opsForZSet().reverseRange(key, strIndex, endIndex);
    }

    /**
     * 从sortedSet中移除某元素，成功返回被删除的元素个数，元素不存在或移除失败返回0L
     *
     * @param key
     * @param value
     * @return
     */
    public Long removeMemberOfZSet(String key, Object... value) {
        return stringRedisTemplate.opsForZSet().remove(key, value);
    }

    /**
     * 从sortedSet中移除某元素，成功返回被删除的元素个数，元素不存在或移除失败返回0L
     * endIndex如果大于sortedSet的size，则返回的是从strIndex开始实际被删除的元素个数
     * 如，size为4，strIndex为0，endIndex为5，则实际删除index为0,1,2,3的元素，返回4L
     *
     * @param key
     * @param strIndex
     * @param endIndex
     * @return
     */
    public Long removeMemberOfZSetRange(String key, long strIndex, long endIndex) {
        return stringRedisTemplate.opsForZSet().removeRange(key, strIndex, endIndex);
    }

    /**
     * 从sortedSet中移除某元素，成功返回被删除的元素个数，元素不存在或移除失败返回0L
     * minScore如果大于maxScore，返回0L
     * minScore和maxScore之间无sortedSet已存在的score，返回0L
     *
     * @param key
     * @param minScore
     * @param maxScore
     * @return
     */
    public Long removeMemberOfZSetRangeByScore(String key, double minScore, double maxScore) {
        return stringRedisTemplate.opsForZSet().removeRangeByScore(key, minScore, maxScore);
    }

    /**
     * 从sortedSet中获取score在minScore和maxScore之间的元素个数
     * minScore如果大于maxScore，返回0L
     * minScore和maxScore之间无sortedSet已存在的score，返回0L
     *
     * @param key
     * @param minScore
     * @param maxScore
     * @return
     */
    public Long count(String key, double minScore, double maxScore) {
        return stringRedisTemplate.opsForZSet().count(key, minScore, maxScore);
    }

    /**
     * 获取sortedSet的元素个数
     *
     * @param key
     * @return
     */
    public Long getSizeOfZSet(String key) {
        return stringRedisTemplate.opsForZSet().size(key);
    }

    /**
     * 获取某元素的score
     *
     * @param key
     * @param value
     * @return
     */
    public Double getScoreOfZSet(String key, String value) {
        return stringRedisTemplate.opsForZSet().score(key, value);
    }

    /**
     * 添加hash值
     *
     * @param key
     * @param hashKey
     * @param hashValue
     */
    public void addElementOfHash(String key, String hashKey, String hashValue) {
        stringRedisTemplate.opsForHash().put(key, hashKey, hashValue);
    }

    /**
     * 获取hash值
     *
     * @param key
     * @param hashKey
     * @return
     */
    public Object getElementOfHash(String key, String hashKey) {
        return stringRedisTemplate.opsForHash().get(key, hashKey);
    }

    /**
     * 添加所有值到hash
     *
     * @param key
     * @param map
     */
    public void addAllElementOfHash(String key, Map<String, String> map) {
        stringRedisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 判断是否存在该hash
     *
     * @param key
     * @param hashKey
     */
    public Boolean exists(String key, String hashKey) {
        return stringRedisTemplate.opsForHash().hasKey(key, hashKey);
    }

    /**
     * 增加hash值
     *
     * @param key
     * @param hashKey
     * @param hashKey
     */
    public Long increment(String key, String hashKey, long count) {
        return stringRedisTemplate.opsForHash().increment(key, hashKey, count);
    }

    /**
     * 获取所有hash值
     *
     * @param key
     * @param hashKeys
     * @return
     */
    public List<Object> getAllElementOfHash(String key, Collection<?> hashKeys) {
        return stringRedisTemplate.opsForHash().multiGet(key, (Collection<Object>) hashKeys);
    }

    /**
     * 获取key对应的所有hash元素
     *
     * @param key
     * @return
     */
    public Map<Object, Object> getAllElementOfHash(String key) {
        return stringRedisTemplate.opsForHash().entries(key);
    }

    /**
     * 获取所有hash的个数
     *
     * @param key
     * @return
     */
    public Long getSizeOfHash(String key) {
        return stringRedisTemplate.opsForHash().size(key);
    }

    /**
     * 检查哈希键值是否存在
     *
     * @param key
     * @param hashKey
     * @return
     */
    public Boolean isExisted(String key, String hashKey) {
        return stringRedisTemplate.opsForHash().hasKey(key, hashKey);
    }

    /**
     * 根据键值获取所有哈希键值
     *
     * @param key
     * @return
     */
    public Set<Object> getHashKeysBy(String key) {
        return stringRedisTemplate.opsForHash().keys(key);
    }

    /**
     * 根据键值获取所有哈希值
     *
     * @param key
     * @return
     */
    public List<Object> getHashValuesBy(String key) {
        return stringRedisTemplate.opsForHash().values(key);
    }

    /**
     * 根据键值获取哈希map
     *
     * @param key
     * @return
     */
    public Map<Object, Object> getHashMapBy(String key) {
        return stringRedisTemplate.opsForHash().entries(key);
    }

    /**
     * 删除某键值中的哈希键值
     *
     * @param key
     * @param hashKey
     */
    public void deleteElementOfHash(String key, Object... hashKey) {
        stringRedisTemplate.opsForHash().delete(key, hashKey);
    }

    /**
     * 返回给定key的有效秒数，如果是-1则表示永远有效
     *
     * @param key
     * @return
     */
    public Long getExpireSecondsBy(String key) {
        return stringRedisTemplate.getExpire(key);
    }

    /**
     * 返回给定key的有效分钟数，如果是-1则表示永远有效
     *
     * @param key
     * @return
     */
    public Long getExpireMinutesBy(String key) {
        return stringRedisTemplate.getExpire(key, TimeUnit.MINUTES);
    }

    /**
     * 返回给定key的有效小时数，如果是-1则表示永远有效
     *
     * @param key
     * @return
     */
    public Long getExpireHoursBy(String key) {
        return stringRedisTemplate.getExpire(key, TimeUnit.HOURS);
    }

    /**
     * 返回给定key的有效天数，如果是-1则表示永远有效
     *
     * @param key
     * @return
     */
    public Long getExpireDaysBy(String key) {
        return stringRedisTemplate.getExpire(key, TimeUnit.DAYS);
    }

    /**
     * 添加元素到set
     *
     * @param key
     * @param value
     */
    public void addElementOfSet(String key, String value) {
        stringRedisTemplate.opsForSet().add(key, value);
    }

    /**
     * 从set获取所有元素
     *
     * @param key
     * @return
     */
    public Set<String> getElementOfSet(String key) {
        return stringRedisTemplate.opsForSet().members(key);
    }

    /**
     * 判断元素是否是set的元素。若是则true，否则false
     *
     * @param key
     * @param value
     * @return
     */
    public boolean isMember(String key, Object value) {
        return stringRedisTemplate.opsForSet().isMember(key, value);
    }

    /**
     * 从set中移除某元素，成功返回1L，元素不存在或移除失败返回0L
     *
     * @param key
     * @param value
     * @return
     */
    public Long removeMemberOfSet(String key, Object... value) {
        return stringRedisTemplate.opsForSet().remove(key, value);
    }

    /**
     * 获取set的元素个数
     *
     * @param key
     * @return
     */
    public Long getSizeOfSet(String key) {
        return stringRedisTemplate.opsForSet().size(key);
    }

    /**
     * 从set中每次读取一条（出栈）
     *
     * @param key
     * @return
     */
    public Object getElementOfSetBy(String key) {
        return stringRedisTemplate.opsForSet().pop(key);
    }

    /**
     * 根据redis的数据库index，获取key值
     * 集群模式，index必定为0，否则报Cannot SELECT non zero index in cluster mode错误
     *
     * @return
     */
    public Long getDBSize() {
        return (Long) stringRedisTemplate.execute((RedisCallback<Object>) connection -> {
            connection.select(0);
            return connection.dbSize();
        });
    }
}