package com.wujunshen.util.redis;

import com.wujunshen.RedisUtilsApplication;
import com.wujunshen.entity.RedisNode;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.SortParameters;
import org.springframework.data.redis.core.query.SortQueryBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.*;

import static java.lang.Thread.sleep;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Author:frankwoo(吴峻申) <br>
 * Date:2017/9/15 <br>
 * Time:上午9:53 <br>
 * Mail:frank_wjs@hotmail.com <br>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RedisUtilsApplication.class)
@EnableAutoConfiguration
@Slf4j
public class RedisUtilsTest {
    private static final int MAX_SIZE = 100;
    //public static final int MAX_SIZE = 65534;
    @Resource
    private RedisUtils redisUtils;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFlushDB() {
        assertThat(redisUtils.flushDB(), is("ok"));
    }

    @Test
    public void testGetNodes() {
        RedisNode redisNode = redisUtils.getNodes();
        assertThat(redisNode.getHostName(), is("localhost"));
        assertThat(redisNode.getPort(), is(6379));
    }

    @Test
    public void testGetValuesByKey() {
        assertThat(redisUtils.getSizeOfList("*"), is(0L));
    }

    @Test
    public void testSet() {
        assertThat(redisUtils.exists("lover"), is(false));
        redisUtils.set("lover", "xxxx");
        assertThat(redisUtils.getBy("lover"), is("xxxx"));
        List<String> keys = redisUtils.getAllValuesBy("lo*");
        assertThat(keys.size(), is(1));
        assertThat(redisUtils.exists("lover"), is(true));
        redisUtils.delete("lover");
    }

    @Test
    public void testSetIfAbsent() {
        assertThat(redisUtils.exists("myName"), is(false));
        redisUtils.setIfAbsent("myName", "frank");
        assertThat(redisUtils.getBy("myName"), is("frank"));
        redisUtils.delete("myName");
    }

    @Test
    public void testAppend() {
        redisUtils.set("myName", "frank");
        assertThat(redisUtils.getBy("myName"), is("frank"));
        assertNotEquals(redisUtils.append("myName", " is me"), 0);
        redisUtils.delete("myName");
    }

    @Test
    public void testSetExpire() throws Exception {
        redisUtils.setExpire("myName", "frank is me", 2L);
        sleep(3000);
        assertNull(redisUtils.getBy("myName"));
        redisUtils.delete("myName");
    }

    @Test
    public void testGetAndSet() {
        redisUtils.set("myName", "frank");
        redisUtils.getAndSet("myName", "wujunshen");
        assertThat(redisUtils.getBy("myName"), is("wujunshen"));
        redisUtils.delete("myName");
    }

    @Test
    public void testGetRangeOf() {
        redisUtils.set("myName", "wujunshen");
        assertThat(redisUtils.getRangeOf("myName", 0, 1), is("wu"));
        redisUtils.delete("myName");
    }

    @Test
    public void testMultiGet() {
        Map<String, String> map = new HashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", "value3");
        map.put("key4", "value4");
        redisUtils.multiSet(map);
        Set<String> set = new HashSet<>();
        set.add("key1");
        set.add("key2");
        set.add("key3");
        set.add("key4");
        assertThat(redisUtils.multiGet(set).size(), is(4));
        assertThat(redisUtils.multiGet(set).get(0), is("value1"));
        redisUtils.deleteMulti(set);
        for (Object value : redisUtils.multiGet(set)) {
            assertNull(value);
        }
    }

    @Test
    public void testGetListBy() {
        redisUtils.addElementOfList("messages", "value1");
        redisUtils.addElementOfList("messages", "value2");
        redisUtils.addElementOfList("messages", "value3");

        List<String> result1 = redisUtils.getListBy("messages", 0, 1);
        List<String> result2 = redisUtils.getListBy("messages", 0, -1);
        assertThat(result1.get(0), is("value3"));
        assertThat(result1.get(1), is("value2"));

        assertThat(result2.get(0), is("value3"));
        assertThat(result2.get(1), is("value2"));
        assertThat(result2.get(2), is("value1"));

        redisUtils.delete("messages");
    }

    @Test
    public void testGetSizeBy() {
        redisUtils.addElementOfList("messages", "value1");
        redisUtils.addElementOfList("messages", "value2");
        redisUtils.addElementOfList("messages", "value3");

        assertThat(redisUtils.getSizeOfList("messages"), is(3L));

        redisUtils.delete("messages");
    }

    @Test
    public void testSortBy() {
        redisUtils.addElementOfList("messages", "frank");
        redisUtils.addElementOfList("messages", "alex");
        redisUtils.addElementOfList("messages", "sam");
        redisUtils.addElementOfList("messages", "hydra");
        redisUtils.addElementOfList("messages", "iron man");

        List<String> result = redisUtils.sortBy("messages");
        assertThat(result.get(0), is("alex"));
        assertThat(result.get(1), is("frank"));
        assertThat(result.get(2), is("hydra"));
        assertThat(result.get(3), is("iron man"));
        assertThat(result.get(4), is("sam"));

        result = redisUtils.sortDescBy("messages");
        assertThat(result.get(0), is("sam"));
        assertThat(result.get(1), is("iron man"));
        assertThat(result.get(2), is("hydra"));
        assertThat(result.get(3), is("frank"));
        assertThat(result.get(4), is("alex"));

        result = redisUtils.sortPageBy("messages", 0, 2);
        assertThat(result.get(0), is("alex"));
        assertThat(result.get(1), is("frank"));

        result = redisUtils.sortPageBy("messages", 1, 2);
        assertThat(result.get(0), is("frank"));
        redisUtils.delete("messages");
    }

    @Test
    public void testGetRangeOfList() {
        redisUtils.addElementOfList("messages", "frank");
        redisUtils.addElementOfList("messages", "alex");
        redisUtils.addElementOfList("messages", "sam");
        redisUtils.addElementOfList("messages", "hydra");
        redisUtils.addElementOfList("messages", "iron man");

        List result = redisUtils.getRangeOfList("messages", 0, 1);
        List result1 = redisUtils.getRangeOfList("messages", 0, -1);

        assertThat(result.get(0), is("iron man"));
        assertThat(result.get(1), is("hydra"));
        assertThat(result1.get(2), is("sam"));
        assertThat(result1.get(3), is("alex"));
        assertThat(result1.get(4), is("frank"));
        redisUtils.delete("messages");
    }

    @Test
    public void testSetElementOfList() {
        redisUtils.addElementOfList("messages", "frank");
        redisUtils.addElementOfList("messages", "alex");
        redisUtils.addElementOfList("messages", "sam");
        redisUtils.addElementOfList("messages", "hydra");
        redisUtils.addElementOfList("messages", "iron man");

        redisUtils.setElementOfList("messages", "吴峻申", 4);
        assertThat(redisUtils.getElementOfList("messages", 4), is("吴峻申"));
        redisUtils.delete("messages");
    }

    @Test
    public void testRemoveElementOfList() {
        redisUtils.addElementOfList("messages", "frank");
        redisUtils.addElementOfList("messages", "alex");
        redisUtils.addElementOfList("messages", "sam");
        redisUtils.addElementOfList("messages", "hydra");
        redisUtils.addElementOfList("messages", "iron man");

        redisUtils.removeElementOfList("messages", "frank", 4);

        List<String> result = redisUtils.getListBy("messages", 0, -1);
        assertThat(result.size(), is(4));

        redisUtils.delete("messages");
    }


    @Test
    public void testRemoveElementOfListExcept() {
        redisUtils.addElementOfList("messages", "frank");
        redisUtils.addElementOfList("messages", "alex");
        redisUtils.addElementOfList("messages", "sam");
        redisUtils.addElementOfList("messages", "hydra");
        redisUtils.addElementOfList("messages", "iron man");

        redisUtils.removeElementOfListExcept("messages", 0, 1);

        List<String> result = redisUtils.getListBy("messages", 0, -1);
        assertThat(result.size(), is(2));

        redisUtils.delete("messages");
    }

    @Test
    public void testGetElementBy() {
        redisUtils.addElementOfList("messages", "frank");
        redisUtils.addElementOfList("messages", "alex");
        redisUtils.addElementOfList("messages", "sam");
        redisUtils.addElementOfList("messages", "hydra");
        redisUtils.addElementOfList("messages", "iron man");

        assertThat(redisUtils.getElementOfListBy("messages"), is("frank"));

        redisUtils.delete("messages");
    }

    @Test
    public void testAddElementOfZSet() {
        redisUtils.addElementOfZSet("hackers", "Alan Kay", 1940);
        redisUtils.addElementOfZSet("hackers", "Richard Stallman", 1953);
        redisUtils.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        redisUtils.addElementOfZSet("hackers", "Claude Shannon", 1916);
        redisUtils.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        redisUtils.addElementOfZSet("hackers", "Alan Turing", 1912);

        Set<String> mySet = redisUtils.getElementOfZSet("hackers", 0, -1);
        assertThat(mySet.contains("Alan Turing"), is(true));
        assertThat(mySet.contains("Richard Stallman"), is(true));
        assertThat(mySet.contains("Yukihiro Matsumoto"), is(true));
        assertThat(mySet.contains("Linus Torvalds"), is(true));
        assertThat(mySet.contains("Alan Kay"), is(true));

        Set<String> mySet1 = redisUtils.getElementOfZSetDesc("hackers", 0, -1);
        assertThat(mySet1.contains("Alan Turing"), is(true));
        assertThat(mySet1.contains("Richard Stallman"), is(true));
        assertThat(mySet1.contains("Yukihiro Matsumoto"), is(true));
        assertThat(mySet1.contains("Linus Torvalds"), is(true));
        assertThat(mySet1.contains("Alan Kay"), is(true));

        redisUtils.delete("hackers");
    }

    @Test
    public void testGetSizeOfZSet() {
        redisUtils.addElementOfZSet("hackers", "Alan Kay", 1940);
        redisUtils.addElementOfZSet("hackers", "Richard Stallman", 1953);
        redisUtils.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        redisUtils.addElementOfZSet("hackers", "Claude Shannon", 1916);
        redisUtils.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        redisUtils.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(redisUtils.getSizeOfZSet("hackers"), is(6L));
        redisUtils.delete("hackers");
    }

    @Test
    public void testGetScoreOfZSet() {
        redisUtils.addElementOfZSet("hackers", "Alan Kay", 1940);
        redisUtils.addElementOfZSet("hackers", "Richard Stallman", 1953);
        redisUtils.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        redisUtils.addElementOfZSet("hackers", "Claude Shannon", 1916);
        redisUtils.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        redisUtils.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(redisUtils.getScoreOfZSet("hackers", "Yukihiro Matsumoto"), is(1965d));
        redisUtils.delete("hackers");
    }

    @Test
    public void testRemoveMemberOfZSet() {
        redisUtils.addElementOfZSet("hackers", "Alan Kay", 1940);
        redisUtils.addElementOfZSet("hackers", "Richard Stallman", 1953);
        redisUtils.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        redisUtils.addElementOfZSet("hackers", "Claude Shannon", 1916);
        redisUtils.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        redisUtils.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(redisUtils.removeMemberOfZSet("hackers", "Yukihiro Matsumoto", "Alan Turing"), is(2L));
        assertThat(redisUtils.removeMemberOfZSet("hackers", "wujunshen", "Linus Torvalds"), is(1L));
        assertThat(redisUtils.removeMemberOfZSet("hackers", "wujunshen"), is(0L));
        redisUtils.delete("hackers");
    }

    @Test
    public void testRemoveMemberOfZSetRange() {
        redisUtils.addElementOfZSet("hackers", "Alan Kay", 1940);
        redisUtils.addElementOfZSet("hackers", "Richard Stallman", 1953);
        redisUtils.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        redisUtils.addElementOfZSet("hackers", "Claude Shannon", 1916);
        redisUtils.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        redisUtils.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(redisUtils.removeMemberOfZSetRange("hackers", 6, 7), is(0L));
        assertThat(redisUtils.removeMemberOfZSetRange("hackers", 0, 1), is(2L));
        assertThat(redisUtils.removeMemberOfZSetRange("hackers", 0, 7), is(4L));
        redisUtils.delete("hackers");
    }

    @Test
    public void testRemoveMemberOfZSetRangeByScore() {
        redisUtils.addElementOfZSet("hackers", "Alan Kay", 1940);
        redisUtils.addElementOfZSet("hackers", "Richard Stallman", 1953);
        redisUtils.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        redisUtils.addElementOfZSet("hackers", "Claude Shannon", 1916);
        redisUtils.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        redisUtils.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(redisUtils.removeMemberOfZSetRangeByScore("hackers", 2016, 2000), is(0L));
        assertThat(redisUtils.removeMemberOfZSetRangeByScore("hackers", 2010, 2010), is(0L));
        assertThat(redisUtils.removeMemberOfZSetRangeByScore("hackers", 2000, 2016), is(0L));
        assertThat(redisUtils.removeMemberOfZSetRangeByScore("hackers", 1940, 1940), is(1L));
        assertThat(redisUtils.removeMemberOfZSetRangeByScore("hackers", 1912, 1915), is(1L));
        assertThat(redisUtils.removeMemberOfZSetRangeByScore("hackers", 1900, 2000), is(4L));
        redisUtils.delete("hackers");
    }

    @Test
    public void testCount() {
        redisUtils.addElementOfZSet("hackers", "Alan Kay", 1940);
        redisUtils.addElementOfZSet("hackers", "Richard Stallman", 1953);
        redisUtils.addElementOfZSet("hackers", "Yukihiro Matsumoto", 1965);
        redisUtils.addElementOfZSet("hackers", "Claude Shannon", 1916);
        redisUtils.addElementOfZSet("hackers", "Linus Torvalds", 1969);
        redisUtils.addElementOfZSet("hackers", "Alan Turing", 1912);

        assertThat(redisUtils.count("hackers", 2016, 2000), is(0L));
        assertThat(redisUtils.count("hackers", 2010, 2010), is(0L));
        assertThat(redisUtils.count("hackers", 2000, 2016), is(0L));
        assertThat(redisUtils.count("hackers", 1940, 1940), is(1L));
        assertThat(redisUtils.count("hackers", 1912, 1915), is(1L));
        assertThat(redisUtils.count("hackers", 1900, 2000), is(6L));
        redisUtils.delete("hackers");
    }

    @Test
    public void testGetElementOfHash() {
        redisUtils.addElementOfHash("employee", "name", "wujunshen");
        redisUtils.addElementOfHash("employee", "age", "37");
        redisUtils.addElementOfHash("employee", "sex", "male");

        assertThat(redisUtils.getElementOfHash("employee", "name"), is("wujunshen"));
        assertThat(redisUtils.getElementOfHash("employee", "age"), is("37"));
        assertThat(redisUtils.getElementOfHash("employee", "sex"), is("male"));

        redisUtils.delete("employee");
    }

    @Test
    public void testAddAllElementOfHash() {
        Map<String, String> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "37");
        map.put("sex", "male");
        redisUtils.addAllElementOfHash("employee", map);

        List<String> hashKeys = new ArrayList<>();
        hashKeys.add("name");
        hashKeys.add("age");

        List valueList = redisUtils.getAllElementOfHash("employee", hashKeys);

        assertThat(valueList.contains("wujunshen"), is(true));
        assertThat(valueList.contains("37"), is(true));
        assertThat(valueList.contains("male"), is(false));

        Set<String> hashKeySet = new LinkedHashSet<>();
        hashKeySet.add("name");
        hashKeySet.add("age");

        valueList = redisUtils.getAllElementOfHash("employee", hashKeySet);

        assertThat(valueList.contains("wujunshen"), is(true));
        assertThat(valueList.contains("37"), is(true));
        assertThat(valueList.contains("male"), is(false));

        redisUtils.delete("employee");
    }

    @Test
    public void testGetSizeOfHash() {
        Map<String, String> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "37");
        map.put("sex", "male");
        redisUtils.addAllElementOfHash("employee", map);

        assertThat(redisUtils.getSizeOfHash("employee"), is(3L));

        redisUtils.delete("employee");
    }

    @Test
    public void testIsExisted() {
        Map<String, String> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "37");
        map.put("sex", "male");
        redisUtils.addAllElementOfHash("employee", map);

        assertThat(redisUtils.isExisted("employee", "name"), is(true));
        assertThat(redisUtils.isExisted("employee", "mobile"), is(false));

        redisUtils.delete("employee");
    }

    @Test
    public void testGetHashKeysBy() {
        Map<String, String> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "37");
        map.put("sex", "male");
        redisUtils.addAllElementOfHash("employee", map);

        Set mySet = redisUtils.getHashKeysBy("employee");
        assertThat(mySet.isEmpty(), is(false));
        assertThat(mySet.contains("name"), is(true));
        assertThat(mySet.contains("age"), is(true));
        assertThat(mySet.contains("sex"), is(true));

        redisUtils.delete("employee");
    }

    @Test
    public void testGetHashValuesBy() {
        Map<String, String> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "37");
        map.put("sex", "male");
        redisUtils.addAllElementOfHash("employee", map);

        List myList = redisUtils.getHashValuesBy("employee");
        assertThat(myList.isEmpty(), is(false));
        assertThat(myList.contains("wujunshen"), is(true));
        assertThat(myList.contains("37"), is(true));
        assertThat(myList.contains("male"), is(true));

        redisUtils.delete("employee");
    }

    @Test
    public void testDeleteElementOfHash() {
        Map<String, String> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "37");
        map.put("sex", "male");
        redisUtils.addAllElementOfHash("employee", map);

        redisUtils.deleteElementOfHash("employee", "age");
        Set<String> hashKeySet = new LinkedHashSet<>();
        hashKeySet.add("name");
        hashKeySet.add("age");
        hashKeySet.add("sex");

        List valueList = redisUtils.getAllElementOfHash("employee", hashKeySet);
        assertThat(valueList.isEmpty(), is(false));
        assertThat(valueList.contains("wujunshen"), is(true));
        assertThat(valueList.contains("37"), is(false));
        assertThat(valueList.contains("male"), is(true));

        redisUtils.delete("employee");
    }

    @Test
    public void testGetHashMapBy() {
        Map<String, String> map = new HashMap<>();
        map.put("name", "wujunshen");
        map.put("age", "37");
        map.put("sex", "male");
        redisUtils.addAllElementOfHash("employee", map);

        Map hashMap = redisUtils.getHashMapBy("employee");
        assertThat(hashMap.isEmpty(), is(false));
        assertThat(hashMap.containsKey("name"), is(true));
        assertThat(hashMap.containsValue("wujunshen"), is(true));
        assertThat(hashMap.containsKey("age"), is(true));
        assertThat(hashMap.containsValue("37"), is(true));
        assertThat(hashMap.containsKey("sex"), is(true));
        assertThat(hashMap.containsValue("male"), is(true));

        redisUtils.delete("employee");
    }

    @Test
    public void testAddElementOfSet() {
        redisUtils.addElementOfSet("sets", "1");
        redisUtils.addElementOfSet("sets", "2");
        redisUtils.addElementOfSet("sets", "3");
        Set mySet = redisUtils.getElementOfSet("sets");

        assertThat(mySet.contains("1"), is(true));
        assertThat(mySet.contains("2"), is(true));
        assertThat(mySet.contains("3"), is(true));
        redisUtils.delete("sets");
    }

    @Test
    public void testIsMember() {
        redisUtils.addElementOfSet("sets", "1");
        redisUtils.addElementOfSet("sets", "2");
        redisUtils.addElementOfSet("sets", "3");

        assertThat(redisUtils.isMember("sets", "3"), is(true));
        assertThat(redisUtils.isMember("sets", "4"), is(false));

        redisUtils.delete("sets");
    }

    @Test
    public void testRemoveMemberOfSet() {
        redisUtils.addElementOfSet("sets", "1");
        redisUtils.addElementOfSet("sets", "2");
        redisUtils.addElementOfSet("sets", "3");

        assertThat(redisUtils.removeMemberOfSet("sets", "3"), is(1L));
        assertThat(redisUtils.removeMemberOfSet("sets", "4"), is(0L));
        assertThat(redisUtils.removeMemberOfSet("sets", "5", "1"), is(1L));
        assertThat(redisUtils.removeMemberOfSet("sets", "6", "7"), is(0L));

        redisUtils.delete("sets");
    }

    @Test
    public void testGetSizeOfSet() {
        redisUtils.addElementOfSet("sets", "1");
        redisUtils.addElementOfSet("sets", "2");
        redisUtils.addElementOfSet("sets", "3");

        assertThat(redisUtils.getSizeOfSet("sets"), is(3L));

        redisUtils.delete("sets");
    }

    @Test
    public void testGetElementOfSetBy() {
        redisUtils.addElementOfSet("sets", "1");
        redisUtils.addElementOfSet("sets", "2");
        redisUtils.addElementOfSet("sets", "3");

        redisUtils.getElementOfSetBy("sets");
        assertThat(redisUtils.getSizeOfSet("sets"), is(2L));
        redisUtils.getElementOfSetBy("sets");
        assertThat(redisUtils.getSizeOfSet("sets"), is(1L));
        redisUtils.getElementOfSetBy("sets");
        assertThat(redisUtils.getSizeOfSet("sets"), is(0L));

        redisUtils.delete("sets");
    }

    @Test
    public void testGetExpireTimeBy() {
        redisUtils.setExpire("employee", "frank", 60 * 60 * 24L);
        assertThat(redisUtils.getExpireSecondsBy("employee"), is(86400L));
        redisUtils.setExpire("employee", "frank", 60 * 60L);
        assertThat(redisUtils.getExpireMinutesBy("employee"), is(59L));
        assertThat(redisUtils.getExpireHoursBy("employee"), is(0L));
        assertThat(redisUtils.getExpireDaysBy("employee"), is(0L));

        redisUtils.delete("employee");
    }

    @Test
    public void testRename() {
        redisUtils.addElementOfList("employee", "frank");
        redisUtils.rename("employee", "newEmployee");

        assertThat(redisUtils.getElementOfListBy("newEmployee"), is("frank"));

        redisUtils.delete("newEmployee");
    }

    @Test
    public void testGetKindOfValue() {
        redisUtils.set("string", "value");
        redisUtils.addElementOfList("list", "value");
        redisUtils.addElementOfSet("set", "value");
        redisUtils.addElementOfZSet("zset", "value", 100);
        redisUtils.addElementOfHash("hash", "hashKey", "hashValue");

        assertThat(redisUtils.getKindOfValue("frank"), is("none"));
        assertThat(redisUtils.getKindOfValue("string"), is("string"));
        assertThat(redisUtils.getKindOfValue("list"), is("list"));
        assertThat(redisUtils.getKindOfValue("set"), is("set"));
        assertThat(redisUtils.getKindOfValue("zset"), is("zset"));
        assertThat(redisUtils.getKindOfValue("hash"), is("hash"));

        redisUtils.delete("string");
        redisUtils.delete("list");
        redisUtils.delete("set");
        redisUtils.delete("zset");
        redisUtils.delete("hash");
    }

    private void initData4PipelineTest() {
        redisUtils.flushDB();
        for (int i = 0; i < MAX_SIZE; i++) {
            redisUtils.addElementOfList("key", i + "");
        }
    }

    @Test
    public void testUnUsePipeline() {
        initData4PipelineTest();

        long start = new Date().getTime();
        for (int i = 0; i < MAX_SIZE; i++) {
            redisUtils.getElementOfListBy("key");
        }
        long end = new Date().getTime();

        log.info("unuse pipeline cost:" + (end - start) + "ms");
    }

    /**
     * redis单点时忽略此方法
     */
    @Ignore
    @Test(expected = UnsupportedOperationException.class)
    public void testUsePipeline() {
        initData4PipelineTest();

        long start = new Date().getTime();
        redisUtils.executePipelined("key");
        long end = new Date().getTime();

        log.info("use pipeline cost:" + (end - start) + "ms");
    }

    /**
     * sort list
     * LIST结合hash的排序
     * 对应的redis客户端命令是： sort userlist by user:*->name get user:*->name
     */
    @Test
    public void testSort2() {
        redisUtils.flushDB();
        redisUtils.addElementOfList("userlist", "33");
        redisUtils.addElementOfList("userlist", "22");
        redisUtils.addElementOfList("userlist", "55");
        redisUtils.addElementOfList("userlist", "11");
        redisUtils.addElementOfList("userlist", "66");

        redisUtils.addElementOfHash("user:66", "name", "66");
        redisUtils.addElementOfHash("user:55", "name", "55");
        redisUtils.addElementOfHash("user:33", "name", "33");
        redisUtils.addElementOfHash("user:22", "name", "79");
        redisUtils.addElementOfHash("user:11", "name", "24");
        redisUtils.addElementOfHash("user:11", "set", "beijing");
        redisUtils.addElementOfHash("user:22", "set", "shanghai");
        redisUtils.addElementOfHash("user:33", "set", "guangzhou");
        redisUtils.addElementOfHash("user:55", "set", "chongqing");
        redisUtils.addElementOfHash("user:66", "set", "xi'an");

        SortQueryBuilder<String> builder = SortQueryBuilder.sort("userlist");
        List<String> result = redisUtils.sortBy(builder);
        for (String item : result) {
            log.info("item...." + item);
        }
    }

    /**
     * sort set
     * SET结合String的排序
     * 对应的redis 命令是 sort tom:friend:list by score:uid:* get # get uid:* get score:uid:*
     */
    @Test
    public void testSort3() {
        redisUtils.flushDB();
        redisUtils.addElementOfSet("tom:friend:list", "123");
        redisUtils.addElementOfSet("tom:friend:list", "456");
        redisUtils.addElementOfSet("tom:friend:list", "789");
        redisUtils.addElementOfSet("tom:friend:list", "101");

        redisUtils.set("score:uid:123", "1000");
        redisUtils.set("score:uid:456", "6000");
        redisUtils.set("score:uid:789", "100");
        redisUtils.set("score:uid:101", "5999");

        redisUtils.set("uid:123", "{'uid':123,'name':'lucy'}");
        redisUtils.set("uid:456", "{'uid':456,'name':'jack'}");
        redisUtils.set("uid:789", "{'uid':789,'name':'jay'}");
        redisUtils.set("uid:101", "{'uid':101,'name':'jolin'}");

        SortQueryBuilder<String> builder = SortQueryBuilder.sort("tom:friend:list");
        builder.order(SortParameters.Order.DESC);

        List<String> result = redisUtils.sortBy(builder);
        for (String item : result) {
            log.info("item...." + item);
        }

        redisUtils.sortByAndStore(builder, "tom:friend:list");
        List<String> elementOfSet = redisUtils.getListBy("tom:friend:list", 0, -1);

        for (String item : elementOfSet) {
            log.info("stored item...." + item);
        }
    }

    @Test
    public void testGetDBsizeBy() {
        redisUtils.flushDB();

        redisUtils.set("key", "value");
        assertThat(redisUtils.getDBSize(), is(1L));
        redisUtils.set("key1", "value");
        assertThat(redisUtils.getDBSize(), is(2L));
    }
}