package com.wujunshen.config;

import com.wujunshen.RedisUtilsApplication;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Author:frankwoo(吴峻申) <br>
 * Date:2017/9/15 <br>
 * Time:上午9:41 <br>
 * Mail:frank_wjs@hotmail.com <br>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RedisUtilsApplication.class)
@EnableAutoConfiguration
public class RedisConfigTest {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSetStringValue() {
        stringRedisTemplate.opsForValue().set("xx", "111", 60 * 60 * 2, TimeUnit.SECONDS);

        assertThat(stringRedisTemplate.opsForValue().get("xx"), equalTo("111"));
    }
}