# Redis工具类（SpringBoot版）

### **启动**
根目录下执行maven命令

    mvn clean install -DskipTests -Dmaven.test.skip=true 
然后执行
    
    mvn spring-boot:run
   
或在IDE里直接启动RedisUtilsApplication类

注：

原代码支持集群，因为个人原因，目前没有云环境进行redis集群部署，所以此项目目前只支持单节点redis。